﻿using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBuild
{
    class Program
    {
        static void Main2(string[] args)
        {
            Console.WriteLine("1 - Train 1");
            Console.WriteLine("2 - Train 2");
            Console.WriteLine("3 - Test");
            var input = Console.ReadKey(true).KeyChar;

            new GameRunnerSad(new TestGame() , 10, 10);

            new SecretProgram(input != '2', input != '1', input-'1');
        }
    }
}
