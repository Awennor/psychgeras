﻿using Microsoft.Xna.Framework;
using Pidroh.TextRendering;
using SadConsole;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBuild
{
    class GameRunnerSad
    {
        public ITextGame game;
        private readonly int width;
        private readonly int height;
        private static Console startingConsole;

        public GameRunnerSad(ITextGame game, int width, int height)
        {
            game.Init(width, height);
            Microsoft.Xna.Framework.Color[] colors = new Microsoft.Xna.Framework.Color[game.GetPalette().HtmlColors.Length];
            for (int i = 0; i < colors.Length; i++)
            {

                System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(game.GetPalette().HtmlColors[i]);
                colors[i] = new Microsoft.Xna.Framework.Color(color.R, color.G, color.B);

            }

            this.game = game;
            this.width = width;
            this.height = height;
            SadConsole.Game.Create("C64.font", width, height);

            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            // Hook the update event that happens each frame so we can trap keys and respond.
            SadConsole.Game.OnUpdate += (GameTime t) =>
            {
                var gr = game.ScreenHolder.Screen;

                gr.Update((float)t.ElapsedGameTime.TotalSeconds);
                var TextBoard = gr.GetBoard();
                //System.Console.WriteLine((float)t.ElapsedGameTime.TotalSeconds);
                if (SadConsole.Global.KeyboardState.KeysPressed.Count > 0)
                {
                    var key = SadConsole.Global.KeyboardState.KeysPressed[0].Character;
                    if (SadConsole.Global.KeyboardState.KeysPressed[0].Key == Microsoft.Xna.Framework.Input.Keys.Escape)
                    {
                        key = (char)SadConsole.Global.KeyboardState.KeysPressed[0].Key;
                    }
                    game.ScreenHolder.Key.InputUnicode = key;
                    //System.Console.WriteLine("cjarrr " + key);
                    //System.Console.WriteLine("cjarrr " + (int)key);
                }
                else
                {
                    game.ScreenHolder.Key.InputUnicode = -1;
                }

                //var t2 = t;

                //gr.Draw(t.ElapsedGameTime.);
                if (SadConsole.Global.KeyboardState.IsKeyReleased(Microsoft.Xna.Framework.Input.Keys.F5))
                {
                    SadConsole.Settings.ToggleFullScreen();
                }

                if (SadConsole.Global.MouseState.LeftClicked)
                {
                    game.ScreenHolder.Mouse.mouseIO.mouseClickHappen = true;
                }
                else
                {
                    game.ScreenHolder.Mouse.mouseIO.mouseClickHappen = false;

                }

                for (int i = 0; i < TextBoard.Width; i++)
                {
                    for (int j = 0; j < TextBoard.Height; j++)
                    {
                        if (startingConsole.Width <= i)
                        {
                            System.Console.WriteLine("overflow");
                            continue;
                        }
                        if (startingConsole.Height <= j)
                        {
                            System.Console.WriteLine("overflow");
                            continue;
                        }
                        string text = TextBoard.CharAt(i, j) + "";

                        Color foreground = colors[TextBoard.TextColor[i, j]];
                        Color background = colors[TextBoard.BackColor[i, j]];
                        startingConsole.Print(i, j, text,
                            foreground,
                            background);
                        //console.Write(j, i, TextBoard.CharAt(i, j), colors[TextBoard.TextColor[i, j]], colors[TextBoard.BackColor[i, j]]); //UNCOMMENT

                        //Console.SetCursorPosition(i, j);
                        //Console.Write(TextBoard.CharAt(i, j));
                    }
                }
            };
        }

        public static void Run()
        {
            // Start the game.
            SadConsole.Game.Instance.Run();
        }

        private void Init()
        {
            // Any custom loading and prep. We will use a sample console for now

            startingConsole = new Console(width, height);
            //startingConsole.FillWithRandomGarbage();
            startingConsole.Fill(new Rectangle(3, 3, 27, 5), null, Color.Black, 0);
            startingConsole.Print(6, 5, "Hello from SadConsole", ColorAnsi.CyanBright);


            // Set our new console as the thing to render and process
            SadConsole.Global.CurrentScreen = startingConsole;

            //startingConsole.MouseButtonClicked += (sender, argus) =>
            //{
            //    var consolePos = argus.MouseState.ConsolePosition;
            //    game.ScreenHolder.Mouse.mouseIO.pos = new Pidroh.BaseUtils.Point2D(consolePos.X, consolePos.Y);
                

            //};
            startingConsole.MouseMove += (sender, argus) =>
            {
                var consolePos = argus.MouseState.ConsolePosition;
                game.ScreenHolder.Mouse.mouseIO.pos = new Pidroh.BaseUtils.Point2D(consolePos.X, consolePos.Y);
            };
        }

    }
}
