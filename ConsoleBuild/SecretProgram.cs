﻿using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBuild
{
    public class SecretProgram : TextScreenN
    {

        Piece[] pieces = new Piece[4];
        Piece[] selection = new Piece[2];
        string[] shapes = new string[] { "Triangle", "Square" };

        string[] colors = new string[] { "Red", "Blue", "Green" };
        char[] shapeSymbol = new char[] { (char)3, '*' };
        List<int> results = new List<int>();
        List<int> rules = new List<int>();
        List<int> desiredRules = new List<int>();
        List<int> desiredResults = new List<int>();
        int rule;
        private int resultNow = 0;

        int[][] sequences = new int[3][];
        Random random = new Random();
        private TextBoard textBoard;
        private readonly bool resultGeneration;
        private readonly bool ruleGeneration;
        private readonly MouseHoverManager mouseHovers;
        private readonly int REDOID = 6;
        private readonly int RESETID = 7;



        public SecretProgram(bool resultGeneration, bool ruleGeneration, int sequenceI)
        {
            this.Init(50,50);
            this.GetBoard().Draw("Blaa", 0,0,2);
            for (int i = 0; i < sequences.Length; i++)
            {
                if (i == 0)
                    sequences[i] = new int[] { 0, 4, 2, 3, 5, 2, 1, 5, 2, 3, 4, 3, 1, 4, 1, 2, 5, 5, 3, 1, 1, 2 };
                if (i == 1)
                    sequences[i] = new int[] { 1, 4, 2, 3, 5, 2, 1, 5, 2, 3, 4, 3, 1, 4, 1, 2, 5, 5, 3, 1, 1, 2 };
                if (i == 2)
                    sequences[i] = new int[] { 2, 4, 2, 3, 5, 2, 1, 5, 2, 3, 4, 3, 1, 4, 1, 2, 5, 5, 3, 1, 1, 2 };
            }
            var sequence = sequences[sequenceI];
            for (int i = 0; i < pieces.Length; i++)
            {
                pieces[i] = new Piece(i % 2, i / 2);
            }
            for (int i = 0; i < selection.Length; i++)
            {
                selection[i] = new Piece(-1, -1);
            }
            {
                int ruleSim = 0;
                int resultSim = 0;
                for (int i = 0; i < sequence.Length / 2; i++)
                {

                    var piece1 = pieces[sequence[i * 2] % pieces.Length];
                    var piece2 = pieces[sequence[i * 2 + 1] % pieces.Length];
                    int lastResult = resultSim;
                    resultSim = SecretProgram.GetRuleResult(ruleSim, piece1, piece2);
                    ruleSim = SecretProgram.GetNextRule(resultSim, piece1, piece2);

                    desiredRules.Add(ruleSim);

                    desiredResults.Add(resultSim);
                }
            }




            this.resultGeneration = resultGeneration;
            this.ruleGeneration = ruleGeneration;

            mouseHovers = new MouseHoverManager(this.mouseIO);
        }

        

        public override void Update(float f)
        {
            textBoard = this.GetBoard();
            textBoard.SetAll(' ', 2, 0);
            textBoard.SetCursorAt(0, 0);

            if (ruleGeneration)
            {
                textBoard.Draw_Cursor("Desired Letters: ");
                textBoard.CursorX = 16;
                for (int i = 0; i < desiredRules.Count; i++)
                {
                    textBoard.Draw_Cursor((char)(desiredRules[i] + 'a'));
                    textBoard.Draw_Cursor(' ');
                }
                textBoard.CursorNewLine(0);
                textBoard.Draw_Cursor("Letters:");
                textBoard.CursorX = 16;
                for (int i = 0; i < rules.Count; i++)
                {
                    textBoard.Draw_Cursor((char)(rules[i] + 'a'));
                    textBoard.Draw_Cursor(' ');

                }
                textBoard.CursorNewLine(0);
            }

            if (resultGeneration)
            {
                textBoard.Draw_Cursor("Letter: ");
                textBoard.Draw_Cursor((char)('a' + rule));
                textBoard.CursorNewLine(0);
            }


            textBoard.Draw_Cursor("Choice: ");
            DrawShape(selection[0]);
            //textBoard.Draw_Cursor("       ");
            DrawShape(selection[1]);
            textBoard.CursorNewLine(0);
            textBoard.CursorNewLine(0);
            textBoard.CursorNewLine(0);
            textBoard.CursorNewLine(0);
            textBoard.CursorNewLine(0);

            if (ruleGeneration)
            {
                textBoard.Draw_Cursor("Result: ");

                switch (resultNow)
                {
                    case 0:
                        textBoard.Draw_Cursor(resultNow + "");
                        textBoard.CursorNewLine(0);
                        break;
                    case 1:
                        textBoard.Draw_Cursor(resultNow + "");
                        textBoard.CursorNewLine(0);
                        break;
                    case -1:

                        break;
                }
                textBoard.CursorNewLine(0);
            }


            if (resultGeneration)
            {
                textBoard.Draw_Cursor("Results:");
                textBoard.CursorX = 16;
                for (int i = 0; i < results.Count; i++)
                {

                    textBoard.Draw_Cursor(results[i] + "");
                    textBoard.Draw_Cursor(' ');

                }

                textBoard.CursorNewLine(0);
                textBoard.Draw_Cursor("Desired Results:");
                textBoard.CursorX = 16;
                for (int i = 0; i < desiredResults.Count; i++)
                {
                    textBoard.Draw_Cursor((desiredResults[i] + ""));
                    textBoard.Draw_Cursor(' ');
                }
            }


            textBoard.CursorNewLine(0);
            textBoard.CursorNewLine(0);
            //textBoard.Draw_Cursor("CONTROLS");
            //textBoard.CursorNewLine(0);
            //for (int i = 0; i < pieces.Length; i++)
            //{
            //    var p = pieces[i];
            //    textBoard.Draw_Cursor((i + 1) + " - ");
            //    DrawShape(p);
            //    textBoard.CursorNewLine(0);
            //}
            //textBoard.Draw_Cursor("Z - Remove all pieces");
            //textBoard.CursorNewLine(0);
            //textBoard.Draw_Cursor("R - Reset");
            //textBoard.CursorNewLine(0);

            var keyChar = this.InputUnicode;
            const int xDisButton = 6;
            const int Y = 30;
            for (int i = 0; i < pieces.Length; i++)
            {

                int x = i * xDisButton;

                textBoard.DrawGrid(x, Y, 5, 5, 2);
                textBoard.DrawChar(shapeSymbol[pieces[i].shape], 2 + i * 6, 32, pieces[i].color + 2);
                mouseHovers.mouseHovers.Add(new MouseHover(new Pidroh.BaseUtils.Rect(x, Y, 5, 5), 0, i));
            }
            string text = "remove pieces";
            int x_ = 0;
            int id = REDOID;
            ShowButton(Y+7, text, x_, id);
            ShowButton(Y+7, "reset", x_+20, RESETID);

            mouseHovers.Update();
            if (mouseHovers.mouseHoversClickDown.Count > 0)
            {
                int clickId = mouseHovers.mouseHoversClickDown[0].id;
                keyChar = '1' + clickId;
                if (clickId == REDOID)
                {
                    keyChar = 'r';
                }
                if (clickId == RESETID)
                {
                    keyChar = 'z';
                }
            }
            switch (keyChar)
            {
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                    for (int i = 0; i < selection.Length; i++)
                    {
                        if (selection[i].shape == -1)
                        {
                            selection[i] = pieces[keyChar - '1'];
                            if (i == 1)
                            {

                                int ruleR = rule;
                                Piece sa = selection[0];
                                Piece sb = selection[1];
                                var oldResultNow = resultNow;
                                resultNow = GetRuleResult(ruleR, sa, sb);


                                int ruleA = GetNextRule(oldResultNow, sa, sb);
                                if (!ruleGeneration)
                                {
                                    ruleA = random.Next(0, 2);
                                }
                                if (!resultGeneration)
                                {
                                    resultNow = random.Next(0, 2);
                                }
                                rule = ruleA;
                                rules.Add(rule);
                                results.Add(resultNow);
                                //for (int j = 0; j < selection.Length; j++)
                                //{
                                //    selection[j] = new Piece(-1, -1);
                                //}


                            }
                            break;
                        }

                    }
                    break;
                case 'z':
                    ResultInput();
                    break;
                case 'r':
                    rules.Clear();
                    results.Clear();
                    rule = 0;
                    ResultInput();
                    break;
                default:
                    break;
            }

        }

        private void ShowButton(int Y, string text, int x_, int id)
        {
            textBoard.DrawGrid(x_, Y, text.Length + 2, 5, 2);
            textBoard.Draw(text, x_ + 1, Y + 2, 2);
            mouseHovers.mouseHovers.Add(new MouseHover(new Pidroh.BaseUtils.Rect(x_, Y, text.Length + 2, 5), 0, id));
        }

        private static int GetNextRule(int result, Piece sa, Piece sb)
        {
            if (result == 0)
            {
                if (sa.shape == sb.shape) return 0;
                return 1;
            }
            else
            {
                if (sa.shape == sb.shape) return 1;
                return 0;
            }

            
            //int ruleA = 0;
            //bool condition = sa.color == sb.color;
            //bool condition2 = sa.shape == sb.shape;
            //bool conditionS = sa.color == 2 || sb.color == 2;
            //if (conditionS) {
            //    condition = !condition;
            //    condition2 = !condition2;
            //} 

            //if (condition && condition2)
            //{
            //    ruleA = 0;
            //}
            //if (condition && !condition2)
            //{
            //    ruleA = 1;
            //}
            //if (!condition && condition2)
            //{
            //    ruleA = 2;
            //}
            //if (!condition && !condition2)
            //{
            //    ruleA = 3;
            //}

            //return ruleA;
        }

        private static int GetRuleResult(int ruleR, Piece sa, Piece sb)
        {
            bool result = false;
            switch (ruleR)
            {
                case 0:
                    result = sa.color == sb.color;
                    break;
                case 1:
                    result = sa.color != sb.color;
                    break;
                case 2:
                    result = sa.shape == sb.shape;
                    break;
                case 3:
                    result = sa.shape != sb.shape;
                    break;
                default:
                    break;
            }
            if (result) return 1;
            return 0;
        }

        private void ResultInput()
        {
            for (int i = 0; i < 2; i++)
            {
                selection[i] = new Piece(-1, -1);

            }
            //resultNow = -1;
        }

        private void DrawShape(Piece p)
        {
            textBoard.DrawGrid(textBoard.CursorX, textBoard.CursorY, 5, 5, 3);
            

            if(p.shape >= 0)
            textBoard.DrawChar(shapeSymbol[p.shape], textBoard.CursorX+2, textBoard.CursorY+2, p.color + 2);
            textBoard.CursorX += 6;

            //if (p.color == -1 || p.shape == -1) textBoard.Draw_Cursor("__________");
            //else
            //    textBoard.Draw_Cursor(colors[p.color] + " " + shapes[p.shape]);
        }
    }

    public struct Piece
    {
        public readonly int shape;
        public readonly int color;

        public Piece(int shape, int color)
        {
            this.shape = shape;
            this.color = color;
        }
    }
}
